require 'byebug'

class Code
  PEGS = {
    "B" => :blue,
    "G" => :green,
    "O" => :orange,
    "P" => :purple,
    "R" => :red,
    "Y" => :yellow
  }

  attr_reader :pegs

  def self.parse(input)
    chars = input.upcase.chars
    chars.each do |char|
      raise ArgumentError, "Invalid input" unless PEGS.key?(char)
    end
    pegs = chars.map { |char| PEGS[char] }
    self.new(pegs)
  end

  def self.random
    pegs = []
    values = PEGS.values
    4.times do
      pegs << values[rand(values.length)]
    end
    self.new(pegs)
  end

  def initialize(pegs)
    @pegs = pegs
  end

  def [](index)
    @pegs[index]
  end

  def exact_matches(code)
    code.pegs.map.with_index { |peg, idx| peg == self[idx] ? 1 : 0 }.reduce(:+)
  end

  def near_matches(code)
    PEGS.values.map do |peg|
      [self.pegs.count(peg), code.pegs.count(peg)].min
    end.reduce(:+) - exact_matches(code)
  end

  def ==(code)
    code.class == Code && self.pegs == code.pegs
  end

end

class Game
  attr_accessor :guess
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def play
    10.times do
      @guess = get_guess
      break if won?(@guess)
      display_matches(@guess)
    end
    if won?(@guess)
      puts "You won! Congratulations!"
    else
      puts "You lose! Better luck next time!"
    end
  end

  def get_guess
    puts "Please enter your guess."
    Code.parse($stdin.gets.chomp)
  end

  def display_matches(code)
    puts "exact matches: #{@secret_code.exact_matches(code)}"
    puts "near matches: #{@secret_code.near_matches(code)}"
  end

  def won?(code)
    @secret_code == code
  end

end
